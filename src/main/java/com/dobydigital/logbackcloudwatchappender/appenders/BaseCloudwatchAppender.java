package com.dobydigital.logbackcloudwatchappender.appenders;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.AppenderBase;
import ch.qos.logback.core.encoder.Encoder;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.internal.StaticCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.logs.AWSLogsClient;
import com.amazonaws.services.logs.model.CreateLogGroupRequest;
import com.amazonaws.services.logs.model.CreateLogStreamRequest;
import com.amazonaws.services.logs.model.InputLogEvent;
import com.amazonaws.services.logs.model.InvalidSequenceTokenException;
import com.amazonaws.services.logs.model.PutLogEventsRequest;
import com.amazonaws.services.logs.model.PutLogEventsResult;
import com.amazonaws.services.logs.model.ResourceAlreadyExistsException;
import org.slf4j.LoggerFactory;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by XaeroDegreaz on 5/27/2016.
 */
public class BaseCloudwatchAppender extends AppenderBase<ILoggingEvent>
{
    private AWSLogsClient awsLogsClient;
    private String awsKey;
    private String awsSecret;
    private String group;
    private String stream;
    private String region;
    private String token;
    private Encoder<ILoggingEvent> encoder;

    @Override
    public void start()
    {
        awsLogsClient = new AWSLogsClient( new StaticCredentialsProvider( new BasicAWSCredentials( awsKey, awsSecret ) ) );
        if ( region != null )
        {
            awsLogsClient.setRegion( Region.getRegion( Regions.fromName( region ) ) );
        }

        CreateLogGroupRequest createLogGroupRequest = new CreateLogGroupRequest( group );
        try
        {
            awsLogsClient.createLogGroup( createLogGroupRequest );
        }
        catch ( ResourceAlreadyExistsException e )
        {
            addInfo( "Log group already exists: group:" + group );
        }

        CreateLogStreamRequest createLogStreamRequest = new CreateLogStreamRequest( group, stream );
        try
        {
            awsLogsClient.createLogStream( createLogStreamRequest );
        }
        catch ( ResourceAlreadyExistsException e )
        {
            addInfo( "Log stream already exists: stream:" + stream );
        }

        try
        {
            send( "Getting the next expected sequenceToken for AwsLogsAppender" );
        }
        catch ( InvalidSequenceTokenException e )
        {
            token = e.getExpectedSequenceToken();
        }
        super.start();
    }

    @Override
    public void stop()
    {
        super.stop();
        LoggerContext loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();
        loggerContext.stop();
    }

    protected void append( ILoggingEvent eventObject )
    {
        try
        {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            try
            {
                //# It's ok to re-initialize the OutputStream of the encoder instance each time we append
                //# because this append method is called in a thread-safe fashion.
                encoder.init( bos );
                encoder.doEncode( eventObject );
                encoder.close();
            }
            catch ( IOException e )
            {
                addError( "Couldn't init encoder", e );
            }
            send( bos.toString() );
        }
        catch ( Exception e )
        {
            e.printStackTrace();
            addError( "Error while sending a message to CloudWatch", e );
        }
    }

    private void send( String message )
    {
        List<InputLogEvent> logEvents = new LinkedList<>();
        logEvents.add( new InputLogEvent().withTimestamp( new Date().getTime() ).withMessage( message ) );

        PutLogEventsRequest putLogEventsRequest = new PutLogEventsRequest( group, stream, logEvents );
        putLogEventsRequest.setSequenceToken( token );

        PutLogEventsResult putLogEventsResult = awsLogsClient.putLogEvents( putLogEventsRequest );
        token = putLogEventsResult.getNextSequenceToken();
    }

    public void setAwsKey( String awsKey )
    {
        this.awsKey = awsKey;
    }

    public void setAwsSecret( String awsSecret )
    {
        this.awsSecret = awsSecret;
    }

    public void setGroup( String group )
    {
        this.group = group;
    }

    public void setStream( String stream )
    {
        this.stream = stream;
    }

    public void setRegion( String region )
    {
        this.region = region;
    }

    public void setEncoder( Encoder encoder )
    {
        this.encoder = encoder;
    }
}
