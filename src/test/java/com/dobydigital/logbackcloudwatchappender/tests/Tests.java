package com.dobydigital.logbackcloudwatchappender.tests;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Tests
{
    private static final Logger log = LoggerFactory.getLogger( Tests.class );
    //# The Thread.sleep() is needed because the test starts up and tears down too fast and all of the async logging doesn't get to complete.
    private static final long SLEEP_TIME = 100;

    @Test
    public void doLogTests() throws Exception
    {
        log.trace( "doLogTests() - Trace." );
        Thread.sleep( SLEEP_TIME );
        log.debug( "doLogTests() - Debug." );
        Thread.sleep( SLEEP_TIME );
        log.info( "doLogTests() - Info." );
        Thread.sleep( SLEEP_TIME );
        log.warn( "doLogTests() - Warn." );
        Thread.sleep( SLEEP_TIME );
        log.error( "doLogTests() - Error." );
        Thread.sleep( SLEEP_TIME );
    }
}
